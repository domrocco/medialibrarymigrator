﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MediaLibraryMigrator.MediaLibraryMigrator.Default" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div>
                    <div>
                        <asp:Label runat="server" AssociatedControlID="txtBasePath" Text="Save Path" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtBasePath" ForeColor="Red" Text="*" />
                        <br />
                        <asp:TextBox runat="server" ID="txtBasePath" />
                    </div>
                    <div>
                        <asp:Button runat="server" ID="btnSubmit" OnClick="btnSubmit_Click" Text="Submit" />
                    </div>
                    <div>
                        <asp:Literal runat="server" ID="litMessage" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
