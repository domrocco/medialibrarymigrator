﻿using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MediaLibraryMigrator.MediaLibraryMigrator
{
    public partial class Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            var mediaItems = GetMediaItems();
            if (!mediaItems.Any())
            {
                litMessage.Text += "No files found<br />";
                return;
            }
            for (int index = 0; index < mediaItems.Count; index++)
            {
                var mediaItem = mediaItems[index];
                try
                {
                    string path = CreateFile(mediaItem);
                    litMessage.Text += (index + 1) + ". " + path + " created<br />";
                }
                catch (Exception ex)
                {
                    litMessage.Text += "File creation failed for " + mediaItem.Name + "<br />";
                    litMessage.Text += ex.Message + "<br />";
                }
            }
        }

        private IList<MediaItem> GetMediaItems()
        {
            var result = new List<MediaItem>();
            var database = Database.GetDatabase("web");
            string query = "fast:/sitecore/media library//*[@@templateid='{0603F166-35B8-469F-8123-E8D87BEDC171}']";
            var mediaItems = database.SelectItems(query).ToList();
            foreach (var item in mediaItems)
            {
                if (Globals.LinkDatabase.GetReferrers(item).Any())
                {
                    result.Add(item);
                }
            }
            return result;
        }

        private string CreateFile(MediaItem mediaItem)
        {
            string existingPath = Server.MapPath(mediaItem.FilePath);
            string mediaUrl = MediaManager.GetMediaUrl(mediaItem);
            string directory = Path.GetDirectoryName(mediaUrl.TrimStart('/'));
            string fileName = Path.ChangeExtension(Path.GetFileName(mediaUrl), ".pdf");
            string basePath = Path.Combine(txtBasePath.Text, directory);
            string newPath = Path.Combine(basePath, fileName);
            Directory.CreateDirectory(basePath);
            File.Copy(existingPath, newPath, true);
            return newPath;
        }
    }
}